import React, { Component } from 'react'

export default class TableGioHang extends Component {
    renderContent = () => {
        return this.props.gioHang.map((item) => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>{item.soLuong}</td>
                    <td>
                        <button onClick={() => { this.props.handleChangeQuantity(item.id, -1) }} className="btn btn-secondary">-</button>
                        <span className="px-2">{item.soLuong}</span>
                        <button onClick={() => { this.props.handleChangeQuantity(item.id, 1) }} className="btn btn-danger">+</button>

                    </td>
                    <td><button
                        onClick={() => {
                            this.props.handleRemoveShoe(item.id)
                        }}
                        className="btn btn-warning">Xoa</button></td>
                </tr>
            )
        })
    }
    render() {
        return (

            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>PRICE</th>
                            <th>QUANTITY</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.renderContent()}
                    </tbody>
                </table>
            </div>
        )
    }
}
