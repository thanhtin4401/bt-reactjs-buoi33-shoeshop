import React, { Component } from 'react'

export default class ItemShoe extends Component {
    // (property) shoeArr: {
    //     id: number;
    //     name: string;
    //     alias: string;
    //     price: number;
    //     description: string;
    //     shortDescription: string;
    //     quantity: number;
    //     image: string;
    //shoeData ~ props
    render() {
        // lay du lieu shoeData
        let { image, name, description } = this.props.shoeData;
        return (
            <div className="col-3">
                <div className="card" style={{ width: '100%' }}>
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">
                            {/* neu chieu dai lon hon 30 thi cat */}
                            {description.length < 30
                                ? description
                                : description.slice(0, 30) + "..."}
                        </p>
                        <button onClick={() => {
                            this.props.handlerAddToCard(this.props.shoeData)
                        }} className="btn btn-primary">Add to card</button>
                    </div>
                </div>
            </div>


        )
    }
}
