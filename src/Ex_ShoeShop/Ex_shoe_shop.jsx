import React, { Component } from 'react'
import { shoeArr } from './Data_shoeSop';
import ItemShoe from './ItemShoe';
import TableGioHang from './TableGioHang';

export default class Ex_shoe_shop extends Component {

    // 
    state = {
        shoeArr: shoeArr,
        gioHang: [],
    };
    renderShoes = () => {
        return this.state.shoeArr.map((item) => {
            return (
                <ItemShoe
                    handlerAddToCard={this.handlerAddToCard}
                    shoeData={item}
                    key={item.id}
                />
            )

        })
    };
    handleRemoveShoe = (idShoe) => {
        let index = this.state.gioHang.findIndex((item) => {
            return (item.id == idShoe);
        })
        if (index !== -1) {
            let cloneGioHang = [...this.state.gioHang];
            cloneGioHang.splice(index, 1);
            this.setState({ gioHang: cloneGioHang });
        }
    };

    handlerAddToCard = (sp) => {
        // let cloneGioHang = [...this.state.gioHang, sp];
        // this.setState({ gioHang: cloneGioHang })
        // th1: sp chưa có trong gior hàng tạo object mới có thêm key số lượng
        // let newSp = { ...sp, soLuong: 1 };
        // th2: sp đã có trong giỏ hàng => dựa vào index, tăng số lượng gioHang[index].soLuong++
        let index = this.state.gioHang.findIndex((item) => {
            return item.id == sp.id;

        });
        let cloneGioHang = [...this.state.gioHang];
        this.setState({
            gioHang: cloneGioHang,
        });
        if (index == -1) {
            let newSp = { ...sp, soLuong: 1 };
            cloneGioHang.push(newSp);
        } else {
            cloneGioHang[index].soLuong++
        }
    };
    handleChangeQuantity = (idShoe, step) => {
        let index = this.state.gioHang.findIndex((item) => {
            return item.id == idShoe;
        })
        let cloneGioHang = [...this.state.gioHang];
        cloneGioHang[index].soLuong += step;
        if (cloneGioHang[index].soLuong == 0) {
            cloneGioHang.splice(index, 1);
        }
        // setstate là thằng bất đồng bộ nên chay sao cùng
        this.setState({ gioHang: cloneGioHang });
    }
    render() {
        console.log("render", this.state.gioHang.length);
        return (
            <div className="container py-5">
                {
                    this.state.gioHang.length > 0 && (
                        <TableGioHang handleRemoveShoe={this.handleRemoveShoe} handleChangeQuantity={this.handleChangeQuantity} gioHang={this.state.gioHang} />
                    )
                }
                <div className="row">{this.renderShoes()}</div>
            </div>
        )
    }
}
